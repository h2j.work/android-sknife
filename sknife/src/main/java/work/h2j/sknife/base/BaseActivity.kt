package work.h2j.sknife.base

import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.view.View
import android.view.Window
import android.view.WindowManager
import androidx.annotation.AnimRes
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.FragmentTransaction
import com.readystatesoftware.systembartint.SystemBarTintManager
import dagger.android.AndroidInjection
import work.h2j.sknife.R

abstract class BaseActivity : AppCompatActivity() {
    abstract val layoutId: Int

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(layoutId)
    }

    protected fun getAnimatedFragmentTransaction(
        @AnimRes enterAnimation: Int = R.anim.enter_from_right_fade_in,
        @AnimRes exitAnimation: Int = R.anim.exit_to_left_fade_out,
        @AnimRes popEnterAnimation: Int = R.anim.enter_from_left_fade_in,
        @AnimRes popExitAnimation: Int = R.anim.exit_to_right_fade_out
    ): FragmentTransaction {
        val transaction = supportFragmentManager.beginTransaction()
        transaction.setCustomAnimations(enterAnimation,exitAnimation,popEnterAnimation,popExitAnimation)
        return transaction
    }

    protected fun changeStatusBarColor(color: Int = Color.TRANSPARENT) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT && Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            window.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
            val tintManager = SystemBarTintManager(this)
            tintManager.isStatusBarTintEnabled = true
            tintManager.setTintColor(color)
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            val window: Window = window
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            window.statusBarColor = color
        }
    }

    protected fun fullscreen() {
        if (Build.VERSION.SDK_INT >= 21) {
            window.decorView.systemUiVisibility =
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
        }
    }

    protected fun hideActionBar() {
        supportActionBar?.hide()
    }
}