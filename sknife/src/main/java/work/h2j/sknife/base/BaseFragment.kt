package work.h2j.sknife.base

import android.content.Context
import android.os.Bundle
import androidx.appcompat.app.ActionBar
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import dagger.android.support.AndroidSupportInjection
import work.h2j.sknife.R
import java.io.InvalidClassException

abstract class BaseFragment : Fragment() {
    abstract val layoutId: Int

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        if (context !is BaseActivity)
            throw InvalidClassException(
                BaseActivity::class.simpleName,
                "Fragments that extends BaseFragment need to be provided by a BaseActivity"
            )
    }

    protected val activity: BaseActivity
        get() = (requireActivity() as BaseActivity)

    protected val actionBar: ActionBar?
        get() = activity.supportActionBar

    protected val animatedFragmentManagerTransaction: FragmentTransaction
        get() {
            val transaction = childFragmentManager.beginTransaction()
            transaction.setCustomAnimations(
                R.anim.enter_from_right_fade_in,
                R.anim.exit_to_left_fade_out,
                R.anim.enter_from_left_fade_in,
                R.anim.exit_to_right_fade_out
            );
            return transaction
        }

    override fun onAttach(context: Context) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }
}