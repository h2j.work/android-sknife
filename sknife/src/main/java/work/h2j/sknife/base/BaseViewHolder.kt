package work.h2j.sknife.base

import android.content.Context
import android.view.View
import androidx.recyclerview.widget.RecyclerView

abstract class BaseViewHolder<TItem>(private val view: View) : RecyclerView.ViewHolder(view) {
    abstract fun bindData(data: TItem?)

    protected fun view(): View {
        return view
    }

    protected fun context(): Context {
        return view.context
    }
}