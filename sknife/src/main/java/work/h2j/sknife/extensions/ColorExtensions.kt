package work.h2j.sknife.extensions

import android.graphics.Color
import kotlin.math.roundToInt

fun Int.getColorWithAlpha(ratio: Float): Int {
    val alpha = (Color.alpha(this) * ratio).roundToInt()
    val r: Int = Color.red(this)
    val g: Int = Color.green(this)
    val b: Int = Color.blue(this)
    return Color.argb(alpha, r, g, b)
}